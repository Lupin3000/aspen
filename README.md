# Aspen

Ansible Pentest Application Deployment

* * *

## Categories:

- ** Information Gathering **
  - hping3
  - nmap
  - mtr

- ** Password Attacks **
  - crunch
  - hydra
  - john the ripper

- ** Stress Testing **
  - siege

- ** Wireless Attacks **
  - aircrack-ng
  - mdk3
  - reaver-wps
  - pixiewps
  - bully

* * *

## Vagrant Usage:

1. check easy_install, make and Vagrant is installed
2. edit "Vagrantfile" for personal settings
3. edit "playbook.yml" for personal settings
4. create virtualenv via "make env"
5. run Vagrant-Ansible provision via "make start"

## Non-Vagrant Usage:

1. check Ansible is installed
2. edit "playbook.yml" or create own playbook(s)
3. create inventory file(s)
4. check ssh access to remote hosts
5. run ansible